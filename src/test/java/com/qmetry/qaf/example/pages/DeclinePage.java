package com.qmetry.qaf.example.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class DeclinePage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		driver.get("/");
	}

	
	@FindBy(locator = "//android.widget.Button[@text='Accept'] | //XCUIElementTypeButton[@name='Accept']")
	public QAFWebElement buttonAccept;
	
	@FindBy(locator = "//android.widget.Button[@text='Decline'] | //XCUIElementTypeButton[@name='Decline']")
	public QAFWebElement buttonDecline;
}
