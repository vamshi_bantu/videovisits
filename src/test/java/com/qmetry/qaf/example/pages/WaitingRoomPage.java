package com.qmetry.qaf.example.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class WaitingRoomPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@Override
    protected void openPage(PageLocator arg0, Object... arg1) {
        driver.get("/");
    }
	
	@FindBy(locator = "//android.widget.Button[@text='While using the app'] | //XCUIElementTypeAlert//XCUIElementTypeButton[@name='OK']")
	public QAFWebElement buttonWhileUsingTheApp;
	
	@FindBy(locator = "//android.widget.TextView[@text='Participants waiting']")
	public QAFWebElement textParticipantsWaiting;
	
	@FindBy(locator = "//android.widget.Button[@text='Leave appointment'] | //XCUIElementTypeButton[@name='Leave appointment']")
	public QAFWebElement buttonLeaveAppointment;
	
	@FindBy(locator = "//android.widget.Button[@text='YES'] | //XCUIElementTypeButton[@name='Leave']")
	public QAFWebElement buttonYes;
	
	@FindBy(locator = "//android.widget.Button[@text='CANCEL']")
	public QAFWebElement buttonCancel;
	
	
	public void waitForPageToLoad() {
		
		buttonWhileUsingTheApp.waitForPresent();
	}

	
}
