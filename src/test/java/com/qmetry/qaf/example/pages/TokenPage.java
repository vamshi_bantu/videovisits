package com.qmetry.qaf.example.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class TokenPage extends WebDriverBaseTestPage<WebDriverTestPage> {
		
	
	@Override
    protected void openPage(PageLocator arg0, Object... arg1) {
        driver.get("/");
    }

	@FindBy(locator = "//android.widget.EditText[@text='Invite Token'] | //XCUIElementTypeTextField[@value='Enter Invitation Token']")
	public QAFWebElement textBoxInviteToken;
	
	@FindBy(locator = "//android.widget.Button[@text='START ADHOC VIDEO VISIT'] | //XCUIElementTypeStaticText[@name='Start Adhoc Video Visit']")
	public QAFWebElement buttonStartVideoVisit;
	
	
	public void startVideoVisit() {
		
		textBoxInviteToken.sendKeys("qfIJFlGSdx654bigy4UWaD4bBgAS10KazKlcdRucYsgiVd6igjWYHQKLTk11mzQindkwELvXE4n75GycRHw");
		buttonStartVideoVisit.click();

	}
}
