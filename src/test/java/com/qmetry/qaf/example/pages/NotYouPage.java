package com.qmetry.qaf.example.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class NotYouPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		driver.get("/");
	}

	@FindBy(locator = "//android.widget.Button[@text='Join'] | //XCUIElementTypeButton[@name='Join']")
	public QAFWebElement buttonJoin;

	@FindBy(locator = "//android.widget.Button[@text='Cancel'] | //XCUIElementTypeButton[@name='Cancel']")
	public QAFWebElement buttonCancel;

	public void waitForPageToLoad() {

		buttonJoin.waitForVisible(5000);
	}

}
