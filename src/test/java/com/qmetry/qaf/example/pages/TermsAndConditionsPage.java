package com.qmetry.qaf.example.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class TermsAndConditionsPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@Override
    protected void openPage(PageLocator arg0, Object... arg1) {
        driver.get("/");
    }
	
	@FindBy(locator = "//android.widget.TextView[@text='Terms & Conditions']")
	public QAFWebElement textTermsAndConditions;
	
	@FindBy(locator = "//android.widget.ImageView[@content-desc='Close'] | //XCUIElementTypeButton[@name='Close']")
	public QAFWebElement closeIcon;
	
	@FindBy(locator = "//android.widget.Button[@text='Accept'] | //XCUIElementTypeStaticText[@name='Accept']")
	public QAFWebElement buttonAccept;
	
	
	public void waitForPageToLoad() {
		
		buttonAccept.waitForVisible(5000);
		}
	
}
