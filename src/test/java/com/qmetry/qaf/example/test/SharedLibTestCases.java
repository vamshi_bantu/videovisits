package com.qmetry.qaf.example.test;

import org.testng.annotations.Test;

import com.qmetry.qaf.example.pages.DeclinePage;
import com.qmetry.qaf.example.pages.NotYouPage;
import com.qmetry.qaf.example.pages.TermsAndConditionsPage;
import com.qmetry.qaf.example.pages.TokenPage;
import com.qmetry.qaf.example.pages.WaitingRoomPage;
import com.qmetry.qaf.example.pages.WelcomeToVideoVisitPage;

public class SharedLibTestCases extends TokenPage {
	
	
	
	@Test
	public void cancelVideoVisit() {
		
		startVideoVisit();
		
		WelcomeToVideoVisitPage welcomeToVideoVisitPage = new WelcomeToVideoVisitPage();
		welcomeToVideoVisitPage.waitForPageToLoad();
		welcomeToVideoVisitPage.buttonDeny.click();
		
		NotYouPage notYouPage = new NotYouPage();
		notYouPage.waitForPageToLoad();
		notYouPage.buttonCancel.click();
		
	}
	
	
	@Test
	public void declineVideoVisit() {  
		
		startVideoVisit();
		
		WelcomeToVideoVisitPage welcomeToVideoVisitPage = new WelcomeToVideoVisitPage();
		welcomeToVideoVisitPage.waitForPageToLoad();
		welcomeToVideoVisitPage.buttonConfirm.click();
		
		TermsAndConditionsPage termsAndConditionsPage = new TermsAndConditionsPage();
		termsAndConditionsPage.waitForPageToLoad();
		termsAndConditionsPage.closeIcon.click();
		
		DeclinePage declinePage = new DeclinePage();
		declinePage.buttonDecline.click();
		
	}
	
	
	@Test
	public void joinVideoVisit2() {
		
		startVideoVisit();
		
		WelcomeToVideoVisitPage welcomeToVideoVisitPage = new WelcomeToVideoVisitPage();
		welcomeToVideoVisitPage.waitForPageToLoad();
		welcomeToVideoVisitPage.buttonDeny.click();
		
		NotYouPage notYouPage = new NotYouPage();
		notYouPage.waitForPageToLoad();
		notYouPage.buttonJoin.click();
		
		TermsAndConditionsPage termsAndConditionsPage = new TermsAndConditionsPage();
		termsAndConditionsPage.waitForPageToLoad();
		termsAndConditionsPage.buttonAccept.click();
		
		WaitingRoomPage waitingRoomPage = new WaitingRoomPage();
		waitingRoomPage.waitForPageToLoad();
		waitingRoomPage.buttonWhileUsingTheApp.click();
		waitingRoomPage.buttonWhileUsingTheApp.click();
		waitingRoomPage.buttonLeaveAppointment.click();
		
		
	}
	
	@Test
	public void joinVideoVisit() {
		
		startVideoVisit();
		
		WelcomeToVideoVisitPage welcomeToVideoVisitPage = new WelcomeToVideoVisitPage();
		welcomeToVideoVisitPage.waitForPageToLoad();
		welcomeToVideoVisitPage.buttonConfirm.click();
		
		TermsAndConditionsPage termsAndConditionsPage = new TermsAndConditionsPage();
		termsAndConditionsPage.waitForPageToLoad();
		termsAndConditionsPage.buttonAccept.click();
		
		WaitingRoomPage waitingRoomPage = new WaitingRoomPage();
		waitingRoomPage.waitForPageToLoad();
		waitingRoomPage.buttonWhileUsingTheApp.click();
		waitingRoomPage.buttonWhileUsingTheApp.click();
		waitingRoomPage.buttonLeaveAppointment.click();
		
		
	}

}
