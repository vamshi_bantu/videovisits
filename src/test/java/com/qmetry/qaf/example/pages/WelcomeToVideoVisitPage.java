package com.qmetry.qaf.example.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class WelcomeToVideoVisitPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@Override
    protected void openPage(PageLocator arg0, Object... arg1) {
        driver.get("/");
    }
	
	
	@FindBy(locator = "//android.widget.Button[@text='Confirm'] | //XCUIElementTypeButton[@name='Confirm']")
	public QAFWebElement buttonConfirm;
	
	@FindBy(locator="//android.widget.Button[@text='Deny'] | //XCUIElementTypeButton[@name='Deny']")
	public QAFWebElement buttonDeny;
	
	
public void waitForPageToLoad() {
		
	buttonConfirm.waitForVisible(5000);
	}
	
	

}
